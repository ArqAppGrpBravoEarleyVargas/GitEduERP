#! /bin/bash
if [ "$(whoami)" != "root" ]; then
    echo "Debe ser root, vuelve a intentar con sudo o su, dependiendo de su instalacion"
else
    /etc/init.d/nginx start
    /etc/init.d/nginx status
    /etc/init.d/postgresql start
    /etc/init.d/postgresql status
    /etc/init.d/mongodb start
    /etc/init.d/mongodb status
fi

