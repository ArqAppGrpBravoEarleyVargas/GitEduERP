#! /usr/bin/bash 
rsync -av --progress django/ /var/lib/docker/volumes/django/_data/
rsync -av --progress static/ /var/lib/docker/volumes/static/_data/

