#!/usr/bin/env bash
mkdir -p django/GitEduERP
rsync -av --progress ../GitEduERP django/
cp ../requirements.txt django/
mkdir -p django/GitEduERP/GitEduERP
cp docker_architecture_settings.py django/GitEduERP/GitEduERP/settings.py
mkdir -p static
rsync -av --progress django/GitEduERP/static/ static/
mkdir -p gitlab/config gitlab/logs gitlab/data

