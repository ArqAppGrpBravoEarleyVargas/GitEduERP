psql -U postgres postgres
CREATE USER "gitEduErpUser" WITH PASSWORD 'G1Tedu3RPu$3r';
CREATE DATABASE "gitEduErpDB" WITH OWNER "gitEduErpUser";
\q
psql -U postgres "gitEduErpDB"
CREATE SCHEMA "gitEduErpApp" AUTHORIZATION "gitEduErpUser";
ALTER USER "gitEduErpUser" SET search_path TO "gitEduErpApp";
\q
psql -U "gitEduErpUser" "gitEduErpDB"
SELECT current_schema();
\q
