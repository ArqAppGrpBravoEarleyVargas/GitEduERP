# GitEduERP

## Dependencias que se deben instalar (probado en Debian y Ubuntu)
```{bash}
apt-get install python-virtualenv postgresql-server-dev-all python3-dev postgresql mongodb
```

## Crear y/o activar entorno virtual con ultima lista de dependencias
```{bash}
source activate.sh
```

## Actualizar Dependencias Actuales con un:
```{bash}
pip3 freeze > requirements.txt
```
desde la carpeta raiz.

## Levantar Servidores Auxiliares:
Para el ambiente de pruebas, ejecutar como super usuario:

(En sistemas Linux que utilizan SysVInit -- los que tienen mas que un año de edad)
```{bash}
./servers_up.initd.sh
```

(En sistemas Linux que utilizan Systemd -- los que tienen menor que un año de edad)
```{bash}
./servers_up.systemd.sh
```

## Editar pg_hba.conf
Editar para permitir acceso local a sus gustos.
```{bash}
find / -iname pg_hba.conf
```
nos encontrará donde esta la carpeta de configuracion de PostgreSQL. Tambien puede realizar un:
```{bash}
su - postgres
echo $PGDATA
```
para encontrar de forma más rapida (suponiendo una instalacion/configuracion normal).
* Si se confie en la seguridad de su sistema operativo, se puede dejar todos los conexiones en trust asi nunca pedirá clave para connectarse.
* Si no tiene confianza, puede poner md5 para que siempre pide clave
* Tambien peer es una buena opcion por aquellos usuarios de la base de datos que tambien tienen el mismo nombre de usuario que un usuario del sistema, por ejemplo el usuario postgres; asi se connecta con:

```{bash}
su - postgres
psql
```
sin contraseña pero ningun otro usuario tenga acceso lo cual provee mayor seguridad a aquello cuenta de superusuario de base de datos.

## Crear Base de Datos Relacional con respetivo Usuario y Schema:
```{bash}
psql -U postgres postgres
```

```{sql}
CREATE USER "gitEduErpUser" WITH PASSWORD 'G1Tedu3RPu$3r';
CREATE DATABASE "gitEduErpDB" WITH OWNER "gitEduErpUser";
\q
```

```{bash}
psql -U postgres "gitEduErpDB"
```

```{sql}
CREATE SCHEMA "gitEduErpApp" AUTHORIZATION "gitEduErpUser";
ALTER USER "gitEduErpUser" SET search_path TO "gitEduErpApp";
\q
```

```{bash}
psql -U "gitEduErpUser" "gitEduErpDB"
```

```{sql}
SELECT current_schema();
\q
```

## Crear Base de Datos NoSQL con respetivo Usuario:
```{bash}
mongo
```

```{json}
use gitEduErpDB
```

Creamos un usuario de MongoDB. En los ultimos versiones se hace con:
```
db.createUser(
   {
     user: "gitEduErpUser",
     pwd: "G1Tedu3RPu$3r",
     roles: [ "readWrite", "dbAdmin" ]
   }
)
```
Pero anteriormente fue asi:
```
db.addUser(
   {
     user: "gitEduErpUser",
     pwd: "G1Tedu3RPu$3r",
     roles: [ "readWrite", "dbAdmin" ]
   }
)
```
De tal forma que se puede probar la primera forma y si no funciona utilizar la segunda forma.

