#! /bin/bash
if [ "$(whoami)" != "root" ]; then
    echo "Debe ser root, vuelve a intentar con sudo o su, dependiendo de su instalacion"
else
    systemctl start nginx
    systemctl status nginx
    systemctl start postgresql
    systemctl status postgresql
    systemctl start mongodb
    systemctl status mongodb
fi

