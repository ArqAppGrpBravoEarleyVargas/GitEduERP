# run with `source activate.sh`
if [ ! -d erpEnv ]; then
	virtualenv --python=python3 erpEnv
fi
source erpEnv/bin/activate
pip3 install -r requirements.txt

