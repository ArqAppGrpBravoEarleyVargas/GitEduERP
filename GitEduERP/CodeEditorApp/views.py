from django.core.exceptions import PermissionDenied
from .forms import CodeForm, CodeGlobalPermissionsForm, AddCollaboratorForm
from django.contrib.auth.models import User
from django.http import Http404, HttpResponse
from django.shortcuts import render, redirect
from django.views import View

from . import mongo_connect
from .gitlab_api import get_gitlab_user, create_gitlab_snippet, get_gitlab_snippet, update_gitlab_snippet
from .mongo_models import CodeModel
from .models import UserCode, CodeCollaborator, CodeEdits, GitlabSnippet # , GitlabUser
from . import constants
import json

# Create your views here.

class ProfileView(View):

    def get(self, request):
        if request.user.is_authenticated:
            username = request.user.username
            return redirect('user-profile', username)
        else:
            return redirect('login')


class UserView(View):

    def get(self, request, user):
        username = user  # self.kwargs['user']
        # get user
        users = User.objects.filter(username=username)
        if len(users) == 0:
            raise Http404("Usuario no existe");
        elif len(users) > 1:
            return HttpResponse(status=500)
        else:
            user_view = users[0]
            i_am_user = False
            if request.user.is_authenticated and request.user == user_view:
                i_am_user = True
            my_codes =[]
            collab_codes = []
            collab_in = CodeCollaborator.objects.filter(user=user_view)
            # gitlab_user = None
            if i_am_user:
                my_codes = UserCode.objects.filter(user_owner=user_view)
                gitlab_user = get_gitlab_user(user=request.user)
                if gitlab_user is None:
                    return HttpResponse(status=500)
            else:
                users_code = UserCode.objects.filter(user_owner=user_view)
                '''collaborators = []
                if request.user.is_authenticated:
                    collaborators = CodeCollaborator.objects.filter(userCode=users_code).filter(user=request.user)'''
                for user_code in users_code:
                    if not user_code.onlyAuthUsers:
                        if user_code.globalCanRead:
                            my_codes.append(user_code)
                    else:
                        if request.user.is_authenticated:
                            if not user_code.onlyCollaborators:
                                if user_code.globalCanRead:
                                    my_codes.append(user_code)
                            else:
                                collaborators = CodeCollaborator.objects.filter(userCode=user_code)\
                                    .filter(user=request.user)
                                #if len(collaborators.filter(userCode=user_code)) != 0:
                                if len(collaborators) != 0:
                                    #for permision in collaborators.filter(userCode=user_code):
                                    for permission in collaborators:
                                        if permission.canRead:
                                            my_codes.append(user_code)
                                            break
            if len(collab_in) > 0:
                for collab in collab_in:
                    user_code = collab.userCode
                    if not user_code.onlyAuthUsers:
                        if user_code.globalCanRead:
                            collab_codes.append(user_code)
                    else:
                        if request.user.is_authenticated:
                            if not user_code.onlyCollaborators:
                                if user_code.globalCanRead:
                                    collab_codes.append(user_code)
                            else:
                                collaborators = CodeCollaborator.objects.filter(userCode=user_code) \
                                    .filter(user=request.user)
                                if len(collaborators) != 0:
                                    for permission in collaborators:
                                        if permission.canRead:
                                            collab_codes.append(user_code)
                                            break
            context = {"user": user_view, "same": i_am_user, 'tiene_codigo': (len(my_codes) > 0), 'mycode': my_codes,
                       'esCollab': (len(collab_codes) > 0), 'collab_codes': collab_codes}
            return render(request, "editor/user.html", context=context)

    '''
    def post(self, request, user):
        # update user
        return redirect(UserView, user)
    '''


class GenericCodeView(View):
    form_class = CodeForm
    global_permission_form_class = CodeGlobalPermissionsForm
    collaborator_form_class = AddCollaboratorForm
    global_permission_initial = {'onlyAuthUsers': True,
               'onlyCollaborators': True,
               'globalCanWrite': False,
               'globalCanRead': False,
               'globalCanExecute': False,
               'globalCanDownload': False
               }
    template = 'editor/code.html'
    editorLangsAndCode = json.dumps(constants.EDITOR_LANGUAGES)
    newCode = None
    gitlab_user = None

class CodeView(GenericCodeView):
    newCode = False

    def get(self, request, id):
        # get code from mongo
        # userCode = UserCode.objects.get(id)
        # codeModels = CodeModel.objects.raw({'cmid': userCode.code_id})
        user_codes = UserCode.objects.filter(code_id=id)
        user_code = None
        if len(user_codes) == 0:
            code_edits = CodeEdits.objects.filter(code_id=id)
            if len(code_edits) == 1:
                code_edit = code_edits[0]
                user_code = code_edit.userCode
            else:
                raise HttpResponse(status=500)
        elif len(user_codes) == 1:
            user_code = user_codes[0]
        else:
            return HttpResponse(status=500)
        if user_code is None:
            return HttpResponse(status=500)
        if user_code.user_owner != request.user:
            if user_code.onlyAuthUsers:
                if not request.user.is_authenticated:
                    raise PermissionDenied("Tiene que estar logeado")
                elif not user_code.onlyCollaborators:
                    if not user_code.globalCanRead:
                        #codeCollaborators = CodeCollaborator.objects.filter(userCode=id, user=request.user)
                        codeCollaborators = CodeCollaborator.objects.filter(userCode=user_code, user=request.user)
                        if len(codeCollaborators) == 0:
                            raise PermissionDenied('No tienes permiso de lectura')
                        elif len(codeCollaborators) > 1:
                            canRead = False
                            for codeCollaborator in codeCollaborators:
                                if codeCollaborator.canRead:
                                    canRead = True
                                    break
                            if not canRead:
                                raise PermissionDenied('No tienes permiso de lectura')
                        else:
                            codeCollaborator = codeCollaborators[0]
                            if not codeCollaborator.canRead:
                                raise PermissionDenied('No tienes permiso de lectura')
                else:
                    #codeCollaborators = CodeCollaborator.objects.filter(userCode=id, user=request.user)
                    codeCollaborators = CodeCollaborator.objects.filter(userCode=user_code, user=request.user)
                    if len(codeCollaborators) == 0:
                        raise PermissionDenied('No tienes permiso de lectura')
                    elif len(codeCollaborators) > 1:
                        canRead = False
                        for codeCollaborator in codeCollaborators:
                            if codeCollaborator.canRead:
                                canRead = True
                                break
                        if not canRead:
                            raise PermissionDenied('No tienes permiso de lectura')
                    else:
                        codeCollaborator = codeCollaborators[0]
                        if not codeCollaborator.canRead:
                            raise PermissionDenied('No tienes permiso de lectura')
            else:
                if not user_code.globalCanRead:
                    codeCollaborators = CodeCollaborator.objects.filter(userCode=user_code, user=request.user)
                    if len(codeCollaborators) == 0:
                        raise PermissionDenied('No tienes permiso de lectura')
                    elif len(codeCollaborators) > 1:
                        canRead = False
                        for codeCollaborator in codeCollaborators:
                            if codeCollaborator.canRead:
                                canRead = True
                                break
                        if not canRead:
                            raise PermissionDenied('No tienes permiso de lectura')
                    else:
                        codeCollaborator = codeCollaborators[0]
                        if not codeCollaborator.canRead:
                            raise PermissionDenied('No tienes permiso de lectura')
        codeModels = CodeModel.objects.raw({'cmid': id})
        if codeModels is None:
            #print("CodeModeles: ")
            if request.user.is_authenticated:
                return redirect(NewCodeView, user=request.user)
            else:
                raise PermissionDenied('Detengase, sus actividades estan siendo monitoreados')
        elif codeModels.count() > 1:
            return HttpResponse(status=500)
        elif codeModels.count() == 0:
            #print("CodeModeles (0)")
            return redirect(NewCodeView, user=request.user)
        else:
            # codeModel = codeModels.get(0)
            codeModel = codeModels[0]
        gitlab_user = get_gitlab_user(user=user_code.user_owner)
        if gitlab_user is None:
            return HttpResponse(status=500)
        code = codeModel.code
        lang = codeModel.language
        file_name = codeModel.file_name
        form = self.form_class(initial={'code': code, 'language': lang, 'file_name': file_name})
        global_perm_form = None
        owner = codeModel.username
        orig = True
        edits = []
        thisEdit = None
        original = None
        modifiedBy = None
        user_code = UserCode.objects.filter(code_id=id)
        collabForm = self.collaborator_form_class()
        if len(list(user_code)) == 0:
            orig = False
            theseEdits = CodeEdits.objects.filter(code_id=id)
            if len(list(theseEdits)) == 0:
                return HttpResponse(status=500)
            else:
                thisEdit = theseEdits[0]
                original = thisEdit.userCode
                modifiedBy = thisEdit.user
        else:
            thisUserCode = user_code[0]
            edits = CodeEdits.objects.filter(userCode=thisUserCode)
            global_perm_form = self.global_permission_form_class(initial={'onlyAuthUsers': thisUserCode.onlyAuthUsers,
                                                                          'onlyCollaborators': thisUserCode.onlyCollaborators,
                                                                          'globalCanWrite': thisUserCode.globalCanWrite,
                                                                          'globalCanRead': thisUserCode.globalCanRead,
                                                                          'globalCanExecute': thisUserCode.globalCanExecute,
                                                                          'globalCanDownload': thisUserCode.globalCanDownload
                                                                          })
        return render(request, self.template, context={'form': form, 'globalPermForm': global_perm_form, 'owner': owner,
                                                       'orig': orig, 'edits': edits, 'thisEdit': thisEdit,
                                                       'original': original, 'modifiedBy': modifiedBy,
                                                       'new': self.newCode, 'id': id,
                                                       'editorLang': self.editorLangsAndCode,
                                                       'collabForm': collabForm})

    def post(self, request, id):
        if not request.user.is_authenticated:
            raise PermissionDenied('No se puede actualizar codigo sin ser autenticado')
        # update code in mongo
        form = self.form_class(request.POST)
        if form.is_valid():
            #userCode = UserCode.objects.get(id)
            codeModels = CodeModel.objects.raw({'cmid': id})
            if codeModels is None:
                return redirect(NewCodeView, user=request.user)
            elif codeModels.count() > 1:
                return HttpResponse(status=500)
            elif codeModels.count() == 0:
                print("CodeModeles (0)")
                return redirect(NewCodeView, user=request.user)
            else:
                #codeModel = codeModels.get(0)
                codeModel = codeModels[0]
            # userCode = None
            userCodes = UserCode.objects.filter(code_id=id)
            if len(userCodes) == 0:
                userCode = UserCode(user_owner=request.user, code_id=id)
            elif len(userCodes) > 1:
                raise HttpResponse(status=500)
            else:
                userCode = userCodes[0]
                if userCode.user_owner != request.user:
                    if userCode.onlyAuthUsers:
                        if not userCode.onlyCollaborators:
                            if not userCode.globalCanWrite:
                                codeCollaborators = CodeCollaborator.objects.filter(userCode=userCode, user=request.user)
                                if len(codeCollaborators) == 0:
                                    raise PermissionDenied('No tienes permiso de escritura')
                                elif len(codeCollaborators) > 1:
                                    canWrite = False
                                    for codeCollaborator in codeCollaborators:
                                        if codeCollaborator.canWrite:
                                            canWrite = True
                                            break
                                    if not canWrite:
                                        raise PermissionDenied('No tienes permiso de escritura')
                                else:
                                    codeCollaborator = codeCollaborators[0]
                                    if not codeCollaborator.canWrite:
                                        raise PermissionDenied('No tienes permiso de escritura')
                        else:
                            codeCollaborators = CodeCollaborator.objects.filter(userCode=userCode, user=request.user)
                            if len(codeCollaborators) == 0:
                                raise PermissionDenied('No tienes permiso de escritura')
                            elif len(codeCollaborators) > 1:
                                canWrite = False
                                for codeCollaborator in codeCollaborators:
                                    if codeCollaborator.canWrite:
                                        canWrite = True
                                        break
                                if not canWrite:
                                    raise PermissionDenied('No tienes permiso de escritura')
                            else:
                                codeCollaborator = codeCollaborators[0]
                                if not codeCollaborator.canWrite:
                                    raise PermissionDenied('No tienes permiso de escritura')
                    else:
                        if not userCode.globalCanWrite:
                            codeCollaborators = CodeCollaborator.objects.filter(userCode=userCode, user=request.user)
                            if len(codeCollaborators) == 0:
                                raise PermissionDenied('No tienes permiso de escritura')
                            elif len(codeCollaborators) > 1:
                                canWrite = False
                                for codeCollaborator in codeCollaborators:
                                    if codeCollaborator.canWrite:
                                        canWrite = True
                                        break
                                if not canWrite:
                                    raise PermissionDenied('No tienes permiso de escritura')
                            else:
                                codeCollaborator = codeCollaborators[0]
                                if not codeCollaborator.canWrite:
                                    raise PermissionDenied('No tienes permiso de escritura')
            gitlab_user = get_gitlab_user(user=userCode.user_owner)
            #if gitlab_user is None:
            #    return HttpResponse(status=500)
            gitlab_snippets = GitlabSnippet.objects.filter(userCode=userCode)
            #if len(gitlab_snippets) == 0:
            #    return HttpResponse(status=500)
            #elif len(gitlab_snippets) > 1:
            #    return HttpResponse(status=500)
            gitlab_snippet = gitlab_snippets[0]
            #if gitlab_snippet is None or gitlab_snippet.snippet_id is None or gitlab_snippet.snippet_id <= -1:
            #    return HttpResponse(status=500)
            remote_gitlab_snippet = get_gitlab_snippet(user=gitlab_user, snippet_id=gitlab_snippet.snippet_id)
            #if remote_gitlab_snippet is None:
            #    return HttpResponse(status=500)
            newCode = form.cleaned_data['code']
            newLang = form.cleaned_data['language']
            newFileName = form.cleaned_data['file_name']
            codeModel.code = newCode
            codeModel.language = newLang
            codeModel.file_name = newFileName
            codeModel.save()
            code = CodeModel(uid=codeModel.uid, orig_cmid=id, username=codeModel.username, code=newCode, language=newLang,
                                file_name=newFileName)
            code_id = code.save().pk
            code.cmid = code_id
            code.save()
            codeEdits = CodeEdits(user=request.user, userCode=userCode, code_id=code_id, language=newLang,
                                  file_name=newFileName)
            codeEdits.save()
            userCode.language = newLang
            userCode.file_name = newFileName
            userCode.save()
            gitlab_snippet = update_gitlab_snippet(user=gitlab_user, snippet=remote_gitlab_snippet, user_code=userCode,
                                                   code=code)
            if gitlab_snippet is None:
                return HttpResponse(status=500)
        #return redirect(CodeView, id)
        return redirect('code', id)


class NewCodeView(GenericCodeView):
    newCode = True

    def get(self, request, user):
        if request.user.is_authenticated:
            user_owners = User.objects.filter(username=user)
            if len(user_owners) == 0:
                raise Http404('Usuario no existe')
            elif len(user_owners) > 1:
                return HttpResponse(status=500)
            else:
                user_owner = user_owners[0]
                if user_owner == request.user:
                    gitlab_user = get_gitlab_user(user=user_owner)
                    if gitlab_user is None:
                        return HttpResponse(status=500)
                    form = self.form_class()
                    global_perm_form = self.global_permission_form_class(initial=self.global_permission_initial)
                    edits = []
                    orig = True
                    return render(request, self.template, context={'form': form, 'globalPermForm': global_perm_form,
                                                                   'owner': user, 'orig': orig, 'edits': edits,
                                                                   'new': self.newCode,
                                                                   'editorLang': self.editorLangsAndCode})
                else:
                    # print("usuario = " + user)
                    # print("request.user" + request)
                    print("Otro usuario")
                    raise PermissionDenied('No se puede guardar un nuevo codigo con otro dueño')
        else:
            print("Falta Autenticar")
            raise PermissionDenied('No se puede guardar codigo si no esta autenticado')

    def post(self, request, user):
        # create code in mongo
        if not request.user.is_authenticated:
            raise PermissionDenied('No se puede guardar codigo si no esta autenticado')
        # if not request.user == user:
        #    raise PermissionDenied('No se puede guardar codigo como otro usuario')
        users = User.objects.filter(username=user)
        if len(users) == 0:
            raise Http404("No se puede guardar codigo para un usuario que no existe")
        elif len(users) > 1:
            return HttpResponse(status=500)
        else:
            u = users[0]
            if request.user == u:
                gitlab_user = get_gitlab_user(user=u)
                if gitlab_user is None:
                    return HttpResponse(status=500)
                uid = u.id
                form = self.form_class(request.POST)
                if form.is_valid():
                    newCode = form.cleaned_data['code']
                    lang = form.cleaned_data['language']
                    file_name = form.cleaned_data['file_name']
                    code = CodeModel(uid=uid, username=user, code=newCode, language=lang, file_name=file_name)
                    code_id = code.save().pk
                    code.cmid = code_id
                    code.orig_cmid = code_id
                    code.save()
                    code_edit = CodeModel(uid=uid, username=user, code=newCode, language=lang, file_name=file_name)
                    code_id_edit = code_edit.save().pk
                    code_edit.cmid = code_id_edit
                    code_edit.orig_cmid = code_id
                    code_edit.save()
                    userCode = UserCode(user_owner=u, code_id=code_id, language=lang, file_name=file_name)
                    userCode.save()
                    # snippets = create_gitlab_snippet(user=u, user_code=userCode)
                    create_gitlab_snippet(user=u, user_code=userCode)
                    codeEdit = CodeEdits(user=u, userCode=userCode, code_id=code_id_edit, language=lang,
                                         file_name=file_name)
                    codeEdit.save()
                    '''if snippets is None or len(snippets) < 2:
                        return HttpResponse(status=500)
                    i = 0
                    for snippet in snippets:
                        if i < 2 and snippet is None:
                            return HttpResponse(status=500)
                        elif i < 2:
                            i += 1
                        else:
                            break
                    #if remote_snippet is None or local_snippet is None:
                    #    return HttpResponse(status=500)'''
                    return redirect('code', code_id)
                else:
                    edits = []
                    orig = True
                    global_perm_form = self.global_permission_form_class(initial=self.global_permission_initial)
                    return render(request, self.template, context={'form': form, 'globalPermForm': global_perm_form,
                                                                   'owner': user, 'orig': orig, 'edits': edits,
                                                                   'new': self.newCode,
                                                                   'editorLang': self.editorLangsAndCode})
            else:
                raise PermissionDenied('No se puede guardar un nuevo codigo con otro dueño')


class GlobalCodePermissionsView(View):
    form = CodeGlobalPermissionsForm

    def post(self, request, id):
        if not request.user.is_authenticated:
            raise PermissionDenied('no se puede modificar permisos sin authenticacion')
        userCodes = UserCode.objects.filter(code_id=id)
        if len(userCodes) == 0:
            return HttpResponse(status=500)
        else:
            userCode = userCodes[0]
            form_data = self.form(request.POST)
            if form_data.is_valid():
                userCode.onlyAuthUsers = form_data.cleaned_data['onlyAuthUsers']
                userCode.onlyCollaborators = form_data.cleaned_data['onlyCollaborators']
                userCode.globalCanWrite = form_data.cleaned_data['globalCanWrite']
                userCode.globalCanRead = form_data.cleaned_data['globalCanRead']
                userCode.globalCanExecute = form_data.cleaned_data['globalCanExecute']
                userCode.globalCanDownload = form_data.cleaned_data['globalCanDownload']
                userCode.save()
            return redirect('code', id)


class CodeCollaboratorsView(View):
    form_class = AddCollaboratorForm

    def get(self, request, id):
        if not request.user.is_authenticated:
            raise PermissionDenied("Usuario no autenticado")
        else:
            userCodes = UserCode.objects.filter(code_id=id)
            if len(userCodes) != 1:
                return HttpResponse(status=500)
            else:
                userCode = userCodes[0]
                if not userCode.user_owner == request.user:
                    raise PermissionDenied("No eres dueño de este codigo y asi no puede visualizar sus permisos")
                else:
                    codeCollaborators = CodeCollaborator.objects.filter(userCode=userCode)
                    return render(request, 'perms/listCollab.html', {'collabs': codeCollaborators, 'userCode': userCode})


class NewCodeCollaboratorsView(CodeCollaboratorsView):
    template = 'perms/newCollab.html'

    def get(self, request, id):
        if request.user.is_authenticated:
            userCodes = UserCode.objects.filter(code_id=id)
            if len(userCodes) != 1:
                return HttpResponse(status=500)
            else:
                userCode = userCodes[0]
                if not userCode.user_owner == request.user:
                    raise PermissionDenied("No eres dueño de este codigo y asi no puede visualizar sus permisos")
                else:
                    collabForm = self.form_class()
                    return render(request, self.template, {'form': collabForm, 'userCode': userCode})
        else:
            raise PermissionDenied("Usuario no autenticado")

    def post(self, request, id):
        if request.user.is_authenticated:
            userCodes = UserCode.objects.filter(code_id=id)
            if len(userCodes) != 1:
                return HttpResponse(status=500)
            else:
                userCode = userCodes[0]
                if not userCode.user_owner == request.user:
                    raise PermissionDenied("No eres dueño de este codigo y asi no puede visualizar sus permisos")
                else:
                    collabForm = self.form_class(request.POST)
                    if collabForm.is_valid():
                        username = collabForm.cleaned_data['user']
                        users = User.objects.filter(username=username)
                        if len(users) == 0:
                            raise Http404('Usuario no existe')
                        elif len(users) == 1:
                            user = users[0]
                            collab = CodeCollaborator(user=user, userCode=userCode,
                                                      canWrite=collabForm.cleaned_data['canWrite'],
                                                      canExecute=collabForm.cleaned_data['canExecute'],
                                                      canRead=collabForm.cleaned_data['canRead'],
                                                      canDownload=collabForm.cleaned_data['canDownload'])
                            collab.save()
                            return redirect('collab-perms-code', id)
                        else:
                            return HttpResponse(status=500)
                    else:
                        return render(request, self.template, {'form': collabForm, 'userCode': userCode})
        else:
            raise PermissionDenied("Usuario no autenticado")


class EditCodeCollaboratorsView(CodeCollaboratorsView):
    template = 'perms/editCollab.html'

    def get(self, request, id):
        if request.user.is_authenticated:
            collabs = CodeCollaborator.objects.filter(id=id)
            if len(collabs) == 0:
                return Http404("No existe")
            elif len(collabs) == 1:
                collab = collabs[0]
                userCode = collab.userCode
                if userCode.user_owner == request.user:
                    collabForm = self.form_class(initial={'user': collab.user.username, 'canWrite': collab.canWrite,
                                                    'canRead': collab.canRead, 'canExecute': collab.canExecute,
                                                    'canDownload': collab.canDownload})
                    return render(request, self.template, {'form': collabForm, 'userCode': userCode})
                else:
                    raise PermissionDenied("No es dueño del codigo para estar editando sus colaboradores")
            else:
                return HttpResponse(status=500)
        else:
            raise PermissionDenied("Usuario debe ser autenticado")

    def post(self, request, id):
        if request.user.is_authenticated:
            collabs = CodeCollaborator.objects.filter(id=id)
            if len(collabs) == 0:
                return Http404("No existe")
            elif len(collabs) == 1:
                collab = collabs[0]
                userCode = collab.userCode
                if userCode.user_owner == request.user:
                    collabForm = self.form_class(request.POST)
                    if collabForm.is_valid():
                        users = User.objects.filter(username=collabForm.cleaned_data['user'])
                        if len(users) == 1:
                            collab.user = users[0]
                        collab.canWrite = collabForm.cleaned_data['canWrite']
                        collab.canRead = collabForm.cleaned_data['canRead']
                        collab.canExecute = collabForm.cleaned_data['canExecute']
                        collab.canDownload = collabForm.cleaned_data['canDownload']
                        collab.save()
                        return redirect('collab-perms-code', userCode.code_id)
                    else:
                        return render(request, self.template, {'form': collabForm, 'userCode': userCode})
                else:
                    raise PermissionDenied("No es dueño del codigo para estar editando sus colaboradores")
            else:
                return HttpResponse(status=500)
        else:
            raise PermissionDenied("Usuario debe ser autenticado")

    def delete(self, request, id):
        if request.user.is_authenticated:
            collabs = CodeCollaborator.objects.filter(id=id)
            if len(collabs) == 0:
                return Http404("No existe")
            elif len(collabs) == 1:
                collab = collabs[0]
                userCode = collab.userCode
                if userCode.user_owner == request.user:
                    collab.delete()
                    return redirect('collab-perms-code', userCode.code_id)
                else:
                    raise PermissionDenied("No es dueño del codigo para estar editando sus colaboradores")
            else:
                return HttpResponse(status=500)
        else:
            raise PermissionDenied("Usuario debe ser autenticado")


class DeleteCodeCollaboratorsView(CodeCollaboratorsView):

    def get(self, request, id):
        #return EditCodeCollaboratorsView.delete(request, id)
        if request.user.is_authenticated:
            collabs = CodeCollaborator.objects.filter(id=id)
            if len(collabs) == 0:
                return Http404("No existe")
            elif len(collabs) == 1:
                collab = collabs[0]
                userCode = collab.userCode
                if userCode.user_owner == request.user:
                    collab.delete()
                    return redirect('collab-perms-code', userCode.code_id)
                else:
                    raise PermissionDenied("No es dueño del codigo para estar editando sus colaboradores")
            else:
                return HttpResponse(status=500)
        else:
            raise PermissionDenied("Usuario debe ser autenticado")

    def delete(self, request, id):
        #return EditCodeCollaboratorsView.delete(request, id)
        if request.user.is_authenticated:
            collabs = CodeCollaborator.objects.filter(id=id)
            if len(collabs) == 0:
                return Http404("No existe")
            elif len(collabs) == 1:
                collab = collabs[0]
                userCode = collab.userCode
                if userCode.user_owner == request.user:
                    collab.delete()
                    return redirect('collab-perms-code', userCode.code_id)
                else:
                    raise PermissionDenied("No es dueño del codigo para estar editando sus colaboradores")
            else:
                return HttpResponse(status=500)
        else:
            raise PermissionDenied("Usuario debe ser autenticado")
