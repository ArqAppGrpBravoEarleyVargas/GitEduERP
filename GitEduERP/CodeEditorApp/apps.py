from django.apps import AppConfig


class CodeeditorappConfig(AppConfig):
    name = 'CodeEditorApp'
