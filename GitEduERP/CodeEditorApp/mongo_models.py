
from pymodm import MongoModel
from pymodm.fields import CharField, IntegerField

from . import constants


class CodeModel(MongoModel):
    cmid = CharField()
    orig_cmid = CharField()
    uid = IntegerField()
    username = CharField()
    code = CharField()
    language = CharField(choices=constants.LANGUAGE_NAMES)
    file_name = CharField()

    class Meta:
        connection_alias = 'nosql'
