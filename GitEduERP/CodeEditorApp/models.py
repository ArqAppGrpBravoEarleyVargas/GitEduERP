from . import constants
from django.contrib.auth.models import User
from django.db import models

# Create your models here.
from django.db.models import Model, ForeignKey, CharField, BooleanField, DateTimeField, BigIntegerField, BigAutoField, \
    IntegerField


class UserCode(Model):
    user_owner = ForeignKey(User, on_delete=models.CASCADE)
    code_id = CharField(max_length=50)  # BigAutoField()
    language = CharField(max_length=50, choices=constants.LANGUAGE_NAMES)
    file_name = CharField(max_length=50)
    onlyAuthUsers = BooleanField(default=True)
    onlyCollaborators = BooleanField(default=True)
    globalCanWrite = BooleanField(default=False)
    globalCanRead = BooleanField(default=False)
    globalCanExecute = BooleanField(default=False)
    globalCanDownload = BooleanField(default=False)


class CodeCollaborator(Model):
    user = ForeignKey(User, on_delete=models.CASCADE)
    userCode = ForeignKey(UserCode, on_delete=models.CASCADE)
    canWrite = BooleanField(default=False)
    canRead = BooleanField(default=False)
    canExecute = BooleanField(default=False)
    canDownload = BooleanField(default=False)

    def __str__(self):
        w_e = "F"
        if self.canWrite:
            w_e = "V"
        r_l = "F"
        if self.canRead:
            r_l = "V"
        e_x = "F"
        if self.canExecute:
            e_x = "V"
        d_d = "F"
        if self.canDownload:
            d_d = "V"
        return "%s : E:%s L:%s X:%s D:%s : %s = %s - %s" % (self.user.username, w_e, r_l, e_x, d_d,
                                                            self.userCode.file_name,
                                                            constants.EDITOR_LANGUAGES[self.userCode.language]['lang'],
                                                            self.userCode.user_owner.username)


class CodeEdits(Model):
    user = ForeignKey(User)
    userCode = ForeignKey(UserCode, on_delete=models.CASCADE)
    timestamp = DateTimeField(auto_now_add=True)
    code_id = CharField(max_length=50)  # BigAutoField()
    language = CharField(max_length=50, choices=constants.LANGUAGE_NAMES)
    file_name = CharField(max_length=50)


class GitlabUser(Model):
    user = ForeignKey(User)
    email = CharField(max_length=80)
    password = CharField(max_length=100, null=False)
    username = CharField(max_length=20, null=False)
    name = CharField(max_length=100)


class GitlabSnippet(Model):
    gitlabUser = ForeignKey(GitlabUser)
    userCode = ForeignKey(UserCode)
    title = CharField(max_length=50, null=False)
    file_name = CharField(max_length=80, null=False)
    code_id = CharField(max_length=50)  # BigAutoField()
    snippet_id = IntegerField(null=False)
