"""GitEduERP URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""

from django.conf.urls import url
from . import views

urlpatterns = [
    url("^$", views.ProfileView.as_view(), name="my-profile"),
    url("^user/(?P<user>\w+)/", views.UserView.as_view(), name="user-profile"),
    url("^new/code/(?P<user>\w+)/", views.NewCodeView.as_view(), name="new-code"),
    url("^code/(?P<id>\w+)/", views.CodeView.as_view(), name="code"),
    url("^perm/global/code/(?P<id>\w+)/", views.GlobalCodePermissionsView.as_view(), name="global-perms-code"),
    url("^perm/collaborator/code/(?P<id>\w+)/", views.CodeCollaboratorsView.as_view(), name="collab-perms-code"),
    url("^perm/new/collaborator/code/(?P<id>\w+)/", views.NewCodeCollaboratorsView.as_view(),
        name="new-collab-perms-code"),
    url("^perm/edit/collaborator/code/(?P<id>\w+)/", views.EditCodeCollaboratorsView.as_view(),
        name="edit-collab-perms-code"),
    url("^perm/delete/collaborator/code/(?P<id>\w+)/", views.DeleteCodeCollaboratorsView.as_view(),
        name="delete-collab-perms-code"),
]