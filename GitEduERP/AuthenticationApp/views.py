from django.contrib.auth.models import User
from .forms import RegistrationForm
from django.shortcuts import render, redirect
from django.views import View

# Create your views here.


class Registration(View):

    form_class = RegistrationForm
    template = 'auth/registration.html'

    def get(self, request):
        form = self.form_class()
        return render(request=request, template_name=self.template, context={'form': form})

    def post(self, request):
        form = self.form_class(request.POST)
        if form.is_valid():
            user = User.objects.create_user(username=form.cleaned_data['username'],
                                            password=form.cleaned_data['password'], email=form.cleaned_data['email'])
            user.first_name = form.cleaned_data['first_name']
            user.last_name = form.cleaned_data['last_name']
            user.save()
            return redirect('login')
        else:
            return render(request=request, template_name=self.template, context={'form': form})
